<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
  <title>购物车</title>
  <%@include file="../common/head.jsp" %>
</head>
<body>

<div id="header">
  <img class="logo_img" alt="" src="static/img/logo.gif">
  <span class="wel_word">订单详情</span>
  <div>
    <span>欢迎<span class="um_span">${sessionScope.user.username }</span>光临尚硅谷书城</span>
    <a href="orderServlet?action=showMyOrders">我的订单</a>
    <a href="userServlet?action=loginOut">注销</a>&nbsp;&nbsp;
    <a href="orderServlet?action=showMyOrders">返回</a>
  </div>
</div>

<div id="main">
  <%--订单不存在--%>
  <c:if test="${empty requestScope.orderItems}">
    <table>
      <tr>
        <td>商品名称</td>
        <td>数量</td>
        <td>单价</td>
        <td>总价</td>
        <td>操作</td>
      </tr>
      <tr>
        <th colspan="5"><a href="index.jsp">亲, 该订单不存在!</a></th>
      </tr>
    </table>
  </c:if>if

  <%--订单存在--%>
  <c:if test="${not empty requestScope.orderItems}">
      <table>
        <tr>
          <td>商品名称</td>
          <td>数量</td>
          <td>单价</td>
          <td>总价</td>
        </tr>
      <c:forEach var="orderItems" items="${requestScope.orderItems}">
        <tr>
          <td>${orderItems.name}</td>
          <td>${orderItems.count}</td>
          <td>${orderItems.price}</td>
          <td>${orderItems.totalPrice}</td>
        </tr>
      </c:forEach>
    </table>
  </c:if>

</div>

<%@include file="../common/footer.jsp" %>
</body>
</html>


