<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>无权访问跳转页面</title>
    <%@ include file="/pages/common/head.jsp"%>
    <style type="text/css">
        h1{
            text-align: center;
            margin-top: 200px;
        }
    </style>
</head>
<body>
<div id="header">
    <img class="logo_img" alt="" src="../../static/img/logo.gif" >
</div>

<div id="main">
    <h1><a href="index.jsp">亲，只有管理员才可以访问该页面，点击跳回主页</a></h1>
</div>


    <%@include file="/pages/common/footer.jsp"%>
</body>
</html>
