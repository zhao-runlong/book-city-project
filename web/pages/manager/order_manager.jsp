<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>订单管理</title>
	<%--静态包含--%>
	<%@ include file="/pages/common/head.jsp"%>
	<style type="text/css">
		a.send_ok{
			font-size: 17px;
		}
	</style>
</head>
<body>

	
	<div id="header">
			<img class="logo_img" alt="" src="../../static/img/logo.gif" >
			<span class="wel_word">订单管理系统</span>
		<%@include file="/pages/common/manage_menu.jsp"%>
	</div>

	<div id="main">
		<table>
			<tr>
				<td>日期</td>
				<td>订单号</td>
				<td>金额</td>
				<td>用户id</td>
				<td>发货</td>
				<td>状态</td>
				<td>详情</td>

			</tr>
			<c:forEach items="${requestScope.orders}" var="orders">
				<tr>
					<td><fmt:formatDate value="${orders.creatTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					<td>${orders.orderId}</td>
					<td>${orders.price}</td>
					<td>${orders.userid}</td>
					<td>
						<c:choose>
							<c:when test="${orders.status==0}">
								<a href="orderServlet?action=sendOrder&orderId=${orders.orderId}">发货</a>
							</c:when>
							<c:otherwise>
								<a class="send_ok">已发货</a>
							</c:otherwise>
						</c:choose>
					</td>
					<td>
						<c:if test="${orders.status==0}">
							<span>未发货</span>
						</c:if>
						<c:if test="${orders.status==1}">
							<span>已发货</span>
						</c:if>
						<c:if test="${orders.status==2}">
							<span>已收货</span>
						</c:if>
					</td>
					<td><a href="orderServlet?action=showManagerOrdersDetail&orderId=${orders.orderId}">查看详情</a></td>
<%--					<td>未发货</td>--%>
				</tr>
			</c:forEach>

		</table>
	</div>

	<%--静态包含页脚--%>
	<%@include file="/pages/common/footer.jsp"%>
</body>
</html>