package com.zrl.pojo;

import java.util.List;

/**
 *page是分页的模型对象
 * @param <T>是具体的模块的javabean类
 */
public class Page<T> {

    public static final Integer PAGE_SIZE = 4;

    private Integer pageNo;

    private Integer pageTotal;

    private Integer pageSize=PAGE_SIZE;

    private Integer getPageTotalCount;

    private List<T> items;

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        if(pageNo > pageTotal){
            pageNo = pageTotal;
        }
        if(pageNo < 1){
            pageNo = 1;
        }
        this.pageNo = pageNo;
    }

    public Integer getPageTotal() {
        return pageTotal;
    }

    public void setPageTotal(Integer pageTotal) {
        this.pageTotal = pageTotal;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getGetPageTotalCount() {
        return getPageTotalCount;
    }

    public void setGetPageTotalCount(Integer getPageTotalCount) {
        this.getPageTotalCount = getPageTotalCount;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "Page{" +
                "pageNo=" + pageNo +
                ", pageTotal=" + pageTotal +
                ", pageSize=" + pageSize +
                ", getPageTotalCount=" + getPageTotalCount +
                ", items=" + items +
                ", url='" + url + '\'' +
                '}';
    }
}
