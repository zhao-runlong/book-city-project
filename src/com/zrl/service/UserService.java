package com.zrl.service;

import com.zrl.pojo.User;

public interface UserService {
    /**
     *注册用户
     * @param user
     */
    public void registUser(User user);

    /**
     * 用户登录
     * @param user
     * @return
     */
    public User login(User user);

    /**
     * 检查用户名是否可用
     * @param username
     * @return 返回TRUE代表用户名不可用，false表示用户名可用
     */
    public boolean UsernameExits(String username);
}
