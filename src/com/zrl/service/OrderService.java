package com.zrl.service;

import com.zrl.pojo.Cart;
import com.zrl.pojo.Order;
import com.zrl.pojo.OrderItem;

import java.util.List;

public interface OrderService {
    public String createOrder(Cart cart,Integer userId);
    public List<Order> showAllOrders();
    public List<Order> showMyOrders(Integer id);
    public List<OrderItem> showOrdersDetail(String orderId);
    public void sendOrder(String orderId,Integer status);
    public void receiveOrder(String orderId,Integer status);

}
