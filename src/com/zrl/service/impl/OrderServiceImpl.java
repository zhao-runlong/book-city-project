package com.zrl.service.impl;

import com.zrl.dao.BookDao;
import com.zrl.dao.OrderDao;
import com.zrl.dao.OrderItemDao;
import com.zrl.dao.impl.BookDaoImpl;
import com.zrl.dao.impl.OrderDaoImpl;
import com.zrl.dao.impl.OrderItemDaoImpl;
import com.zrl.pojo.*;
import com.zrl.service.OrderService;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class OrderServiceImpl implements OrderService {
    private OrderDao orderDao = new OrderDaoImpl();
    private OrderItemDao orderItemDao = new OrderItemDaoImpl();
    private BookDao bookDao = new BookDaoImpl();

    @Override
    public String createOrder(Cart cart, Integer userId) {
        //订单唯一
        String orderId = System.currentTimeMillis()+""+userId;
        //创建订单对象
        Order order = new Order(orderId,new Date(),cart.getTotalPrice(),0,userId);
        //保存订单
        orderDao.saveOrderItem(order);

        //遍历购物车中的每一个商品项转换成为订单项保存到数据库
        for(Map.Entry<Integer, CartItem>entry:cart.getItems().entrySet()){
            //获取购物车中每一个商品项
            CartItem cartItem = entry.getValue();
            //转化为每一个订单项
            OrderItem orderItem = new OrderItem(null,cartItem.getName(),cartItem.getCount(),cartItem.getPrice(),cartItem.getTotalPrice(),orderId);
            //保存订单项到数据库
            orderItemDao.saveOrderItem(orderItem);

            Book book = bookDao.queryBookById(cartItem.getId());
            book.setSales(book.getSales()+cartItem.getCount());
            book.setStock(book.getStock()-cartItem.getCount());
            bookDao.updateBook(book);
        }
        //清空购物车
        cart.clear();
        return orderId;
    }

    @Override
    public List<Order> showAllOrders() {
        return orderDao.queryOrders();
    }

    @Override
    public List<Order> showMyOrders(Integer id) {
        return orderDao.queryOrdersByUserId(id);
    }

    @Override
    public List<OrderItem> showOrdersDetail(String orderId) {
        return orderItemDao.queryOrdersByOrderId(orderId);
    }

    @Override
    public void sendOrder(String orderId,Integer status) {
        orderDao.changeOrderStatus(orderId,1);
    }
    @Override
    public void receiveOrder(String orderId,Integer status) {
        orderDao.changeOrderStatus(orderId,2);
    }

}
