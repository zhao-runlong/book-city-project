package com.zrl.service.impl;

import com.zrl.dao.UserDao;
import com.zrl.dao.impl.UserDaoImpl;
import com.zrl.pojo.User;
import com.zrl.service.UserService;

public class UserServiceImpl implements UserService {

    UserDao userDao = new UserDaoImpl();

    @Override
    public void registUser(User user) {
        userDao.save(user);
    }

    /**
     *
     * @param user
     * @return 返回null是登录失败，返回有值是，登陆成功
     */
    @Override
    public User login(User user) {
        return userDao.queryUserByUsernameAndPassword(user.getUsername(), user.getPassword());
    }

    @Override
    public boolean UsernameExits(String username) {
        if (userDao.queryUserByUsername(username)==null) {
            return false;
        }
        return true;
    }
}
