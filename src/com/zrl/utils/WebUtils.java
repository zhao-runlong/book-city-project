package com.zrl.utils;

import com.zrl.pojo.User;
import org.apache.commons.beanutils.BeanUtils;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;
public class WebUtils extends HttpServlet {

        public static <T> T  copyParamToBean(Map value, T bean){
            try {
//                System.out.println("注入之前");
                BeanUtils.populate(bean,value);
//                System.out.println("注入之后");
            } catch (Exception e){
                e.printStackTrace();
            }

            return bean;
        }

    /**
     * 将字符串转化为int类型的数据
     * @param strInt
     * @param defaultvalue
     * @return
     */
        public static int parseInt(String strInt,int defaultvalue){
            try {
                return Integer.parseInt(strInt);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return defaultvalue;
        }
}
