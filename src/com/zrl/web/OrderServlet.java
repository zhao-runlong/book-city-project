package com.zrl.web;

import com.zrl.pojo.Cart;
import com.zrl.pojo.Order;
import com.zrl.pojo.OrderItem;
import com.zrl.pojo.User;
import com.zrl.service.OrderService;
import com.zrl.service.impl.OrderServiceImpl;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class OrderServlet extends BaseServlet{

    private OrderService orderService = new OrderServiceImpl();

    protected void createOrder(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cart cart = (Cart) req.getSession().getAttribute("cart");
        User loginUser = (User) req.getSession().getAttribute("user");
        if(loginUser==null){
            req.getRequestDispatcher("/pages/user/login.jsp").forward(req,resp);
            return;
        }
        Integer userId = loginUser.getId();
        String orderId = orderService.createOrder(cart, userId);
        req.getSession().setAttribute("orderId",orderId);
        //请求转发到/pages/cart/checkout.jsp
        resp.sendRedirect  (req.getContextPath()+"/pages/cart/checkout.jsp");
    }


    protected void showAllOrders(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

            //调用Service来获取所有的订单项,管理员查看所有订单
            List<Order> orders = orderService.showAllOrders();
            //保存到request域中
            req.setAttribute("orders", orders);
            //转发到order_manager.jsp显示数据
            req.getRequestDispatcher("/pages/manager/order_manager.jsp").forward(req,resp);

    }
    protected void showMyOrders(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        User user = (User) req.getSession().getAttribute("user");
        System.out.println(user);
        //根据用户id查询所有订单
        List<Order> orders = orderService.showMyOrders(user.getId());
        //保存到request域中
        req.setAttribute("orders", orders);
        //转发到order_manager.jsp显示数据
        req.getRequestDispatcher("/pages/order/order.jsp").forward(req,resp);

    }
    protected void showManagerOrdersDetail(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String orderId = req.getParameter("orderId");
//        System.out.println(orderId);
        //调用Service来获取所有的订单项,查看所有订单
        List<Order> orders = orderService.showAllOrders();
        //根据用户orderId查看订单详情
        List<OrderItem> orderItems = orderService.showOrdersDetail(orderId);
//        System.out.println(orderItems);
        //保存到request域中
        req.setAttribute("orders", orders);
        req.setAttribute("orderItems", orderItems);
        //转发到order_manager.jsp显示数据
        req.getRequestDispatcher("/pages/order/order_manager_detail.jsp").forward(req,resp);

    }
    protected void showOrdersDetail(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String orderId = req.getParameter("orderId");
//        System.out.println(orderId);
        //调用Service来获取所有的订单项,查看所有订单
        List<Order> orders = orderService.showAllOrders();
        //根据用户orderId查看订单详情
        List<OrderItem> orderItems = orderService.showOrdersDetail(orderId);
//        System.out.println(orderItems);
        //保存到request域中
        req.setAttribute("orders", orders);
        req.setAttribute("orderItems", orderItems);
        //转发到order_manager.jsp显示数据
        req.getRequestDispatcher("/pages/order/order_detail.jsp").forward(req,resp);
    }

    protected void sendOrder(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String orderId = req.getParameter("orderId");
        orderService.sendOrder(orderId,1);
        //重定向回订单管理页面
        resp.sendRedirect(req.getContextPath()+"/orderServlet?action=showAllOrders");
//        req.getRequestDispatcher("/orderServlet?action=showAllOrders").forward(req,resp);
    }

    protected void receiveOrder(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String orderId = req.getParameter("orderId");
        orderService.receiveOrder(orderId,2);
        //重定向回订单管理页面
        resp.sendRedirect(req.getContextPath()+"/orderServlet?action=showMyOrders");
    }
}
