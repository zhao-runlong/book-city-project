package com.zrl.dao.impl;

import com.zrl.dao.OrderDao;
import com.zrl.pojo.Order;

import java.util.List;

public class OrderDaoImpl extends BaseDao implements OrderDao{
    @Override
    public int saveOrderItem(Order order) {
        String sql = "insert into t_order(`order_id`,`create_time`,`price`,`status`,`user_id`) values(?,?,?,?,?)";
        return update(sql,order.getOrderId(),order.getCreatTime(),order.getPrice(),order.getStatus(),order.getUserid());
    }

    @Override
    public List<Order> queryOrders() {
        String sql ="select order_id orderId,create_time creatTime ,price,status,user_id userId from t_order";
        return queryForList(Order.class,sql);
    }

    @Override
    public List<Order> queryOrdersByUserId(Integer id) {
        String sql= "select order_id orderId,create_time creatTime ,price,status from t_order where user_id = ?";
        return queryForList(Order.class,sql,id);
    }

    @Override
    public int changeOrderStatus(String orderId, Integer status) {
        String sql="update t_order set status=? where order_id = ?";
        return update(sql,status,orderId);
    }

}
