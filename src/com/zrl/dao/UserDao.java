package com.zrl.dao;

import com.zrl.pojo.User;

public interface UserDao {



    /**
     * 通过通过用户名查询用户信息
     * @param username 用户名
     * @return如果返回null，证明没有这个用户！反之亦然！
     */
    public User queryUserByUsername(String username);

    /**
     * 通过用户名和密码查询用户信息
     * @param name 用户名
     * @param password ，密码
     * @return 如果返回null！证明
     */
    public User queryUserByUsernameAndPassword(String name,String password);

    /**
     * 保存信息
     * @param user 用户
     * @return
     */
    public int save(User user);

}
