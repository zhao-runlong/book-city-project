package com.zrl.dao;

import com.zrl.pojo.OrderItem;

import java.util.List;

public interface OrderItemDao {
    public int saveOrderItem(OrderItem orderItem);
    public List<OrderItem> queryOrdersByOrderId(String orderId);
}
