package com.zrl.dao;

import com.zrl.pojo.Order;

import java.util.List;


public interface OrderDao {
    public int saveOrderItem(Order order);
    public List<Order> queryOrders();
    public List<Order> queryOrdersByUserId(Integer id);
    public int  changeOrderStatus(String orderId,Integer status);

}
