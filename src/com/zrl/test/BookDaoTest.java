package com.zrl.test;

import com.zrl.dao.BookDao;
import com.zrl.dao.impl.BookDaoImpl;
import com.zrl.pojo.Book;
import com.zrl.pojo.Page;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

public class BookDaoTest {
    private BookDao bookDao = new BookDaoImpl();

    @Test
    public void addBook() {
        bookDao.addBook(new Book(null,"cydssb","cyd",new BigDecimal(100),1000,0,null));
    }

    @Test
    public void deleteBookById() {
        bookDao.deleteBookById(20);
    }

    @Test
    public void updateBook() {
        bookDao.updateBook(new Book(21,"cydswoer","cyd",new BigDecimal(1000),1000,0,null));
    }

    @Test
    public void queryBookById() {
        System.out.println(bookDao.queryBookById(21));
    }

    @Test
    public void queryBooks() {
        for (Book querybook: bookDao.queryBooks()) {
            System.out.println(querybook);
        }
    }
    @Test
    public void queryForPageTotalCount() {
        System.out.println(bookDao.queryForPageTotalCount());
    }
    @Test
    public void queryForPageTotalCountByPrice() {
        System.out.println(bookDao.queryForPageTotalCountByPrice(0,50));
    }

    @Test
    public void queryBookForItemsByPrice() {
        for (Book book : bookDao.queryForPageItemsByPrice(0, Page.PAGE_SIZE,0,50)) {
            System.out.println(book);
        }
    }
}