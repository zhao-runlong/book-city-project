package com.zrl.test;

import com.zrl.dao.OrderItemDao;
import com.zrl.dao.impl.OrderItemDaoImpl;
import com.zrl.pojo.Order;
import com.zrl.pojo.OrderItem;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

public class OrderItemDaoTest {

    @Test
    public void saveOrderItem() {
        OrderItemDao orderItemDao = new OrderItemDaoImpl();
        orderItemDao.saveOrderItem(new OrderItem(null,"赌神",1,new BigDecimal(100),new BigDecimal(100),"123456789"));
        orderItemDao.saveOrderItem(new OrderItem(null,"java从入门到精通",2,new BigDecimal(100),new BigDecimal(100),"123456789"));
        orderItemDao.saveOrderItem(new OrderItem(null,"蛋炒饭",1,new BigDecimal(100),new BigDecimal(100),"123456789"));

    }
    @Test
    public void queryOrdersByOrderId() {
        OrderItemDao orderItemDao = new OrderItemDaoImpl();
//        String s = "16367262858381";
        List<OrderItem> orderItems = orderItemDao.queryOrdersByOrderId("16367262858381");
        System.out.println(orderItems);
    }
}