package com.zrl.test;

import com.zrl.pojo.Cart;
import com.zrl.pojo.CartItem;
import com.zrl.service.OrderService;
import com.zrl.service.impl.OrderServiceImpl;
import org.junit.Test;

import java.math.BigDecimal;
public class OrderServiceTest {

    @Test
    public void createOrder() {
        Cart cart = new Cart();
        cart.addItem(new CartItem(1,"java入门",1,new BigDecimal(1000),new BigDecimal(1000)));
        cart.addItem(new CartItem(1,"java入门",1,new BigDecimal(1000),new BigDecimal(1000)));
        cart.addItem(new CartItem(2,"数据结构与算法",1,new BigDecimal(100),new BigDecimal(100)));

        OrderService orderService = new OrderServiceImpl();
        orderService.createOrder(cart,1);
    }

    @Test
    public void showAllOrders() {
        OrderService orderService = new OrderServiceImpl();
        System.out.println(orderService.showAllOrders());
    }
    @Test
    public void showMyOrders() {
        OrderService orderService = new OrderServiceImpl();
        System.out.println(orderService.showMyOrders(1));
    }
    @Test
    public void queryOrdersByOrderId() {
        OrderService orderService = new OrderServiceImpl();
        System.out.println(orderService.showOrdersDetail("16367262858381"));
    }
    @Test
    public void sendOrder() {
        OrderService orderService = new OrderServiceImpl();
        orderService.sendOrder("16367262858381",1);
    }
    @Test
    public void receiveOrder() {
        OrderService orderService = new OrderServiceImpl();
        orderService.sendOrder("16367262858381",2);
    }

}