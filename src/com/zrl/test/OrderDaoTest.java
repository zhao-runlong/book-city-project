package com.zrl.test;

import com.zrl.dao.OrderDao;
import com.zrl.dao.impl.OrderDaoImpl;
import com.zrl.pojo.Order;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.Assert.*;

public class OrderDaoTest {

    @Test
    public void saveOrderItem() {
        OrderDao orderDao = new OrderDaoImpl();
        orderDao.saveOrderItem(new Order("123456789",new Date(),new BigDecimal(100),0,1));
    }

    @Test
    public void queryOrders() {
        OrderDao orderDao = new OrderDaoImpl();
        for(Order showOrders : orderDao.queryOrders()){
            System.out.println(showOrders);
        }
    }
    @Test
    public void queryOrdersById() {
        OrderDao orderDao = new OrderDaoImpl();
//        for (Order queryOrdersById : orderDao.queryOrdersByUserId(1)){
//            System.out.println(queryOrdersById);
//        }
        System.out.println(orderDao.queryOrdersByUserId(1));
    }

    @Test
    public void changeOrderStatus() {
        OrderDao orderDao = new OrderDaoImpl();
        orderDao.changeOrderStatus("16368102934061",1);
    }
}